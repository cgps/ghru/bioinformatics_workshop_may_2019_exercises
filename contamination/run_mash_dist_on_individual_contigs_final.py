#!/usr/bin/python

### Import Modules ###

import argparse
import sys
import csv
from Bio import SeqIO
import glob
import os
import shutil




### Run MASH on each contig and combine *distances.tab files into one file (function returns this file)

def run_mash():

	records = SeqIO.parse(assembly_file,"fasta")
	for contig in records:
		contig_id = contig.id
		contig_seq = str(contig.seq)

		individual_contig_file = output_folder + "/" + contig_id + ".fasta"
		with open(individual_contig_file,"a") as contig_output:
			contig_output.write(">")
			contig_output.write(contig_id)
			contig_output.write("\n")
			contig_output.write(contig_seq)
			contig_output.write("\n")

		mash_command = "mash dist" + " -v 0.01 " + "refseq-bacteria-k21-s1000.msh " + individual_contig_file + " > " + individual_contig_file + ".msh.distances.tab"
		os.system(mash_command)
		os.remove(individual_contig_file)
	records.close()
	
	
	
	file_list = []

	for file in glob.glob(output_folder+"/*distances.tab"):
		file_list.append(file)


	all_contigs_results_file = output_folder + "/All_contig_results.txt"

	with open(all_contigs_results_file, 'w') as outfile:
	    for fname in file_list:
        	with open(fname) as infile:
        	    for line in infile:
                	outfile.write(line)
			
	for each_distances_file in glob.glob(output_folder+"/*distances.tab"):
		os.remove(each_distances_file)
			
	return all_contigs_results_file
	
	
	
			
### Find best species match for each contig with hits and outputs to summary file

def best_species_match():


	for key, value in giant_dict.iteritems():
		max_value = max(value)
		contig_name = key.split("/")[1]
		contig_name = contig_name.split(".fasta")[0]

		contig_length = contig_size_dict.get(contig_name)
				
		with open(mash_results_file,"rU") as input_file:
			reader = csv.reader(input_file,delimiter="\t")
			for row in reader:
				hit = row[0]
				query_contig = row[1]
				if query_contig == key:
					mash_distance = row[2]
					pvalue = row[3]
					matching_hashes = row[4]
					matching_hashes = matching_hashes.split("/")[0]
					matching_hashes = int(matching_hashes)
					
					if matching_hashes == max_value:
					
						taxa = taxa_dict.get(hit)

						output_to_summary = contig_name + "\t" + hit + "\t" + mash_distance + "\t" + pvalue + "\t" + str(matching_hashes) + "\t" + taxa + "\t" + str(contig_length) + "\n"
						with open(summary_file,"a") as output_summary:
							output_summary.write(output_to_summary)	
						break



### Finds contigs with no hits and outputs to summary file

def missing_contigs():
	
	missing_count_dict = {}

	for assembly_contig, contig_size in contig_size_dict.iteritems():
		#assembly_contig_fasta = assembly_contig + ".fasta"	
	
		z = 0

		for each_contig, hashes_value in giant_dict.iteritems():
			if assembly_contig in each_contig:
				z = z + 1
		if z == 0:
			missing_count_dict[assembly_contig] = contig_size


	for missing_contig, missing_contig_length in missing_count_dict.iteritems():
		output_for_missing_contigs = missing_contig + "\t" + "No hit" + "\t" + "NA" + "\t" + "NA" + "\t" + "NA" + "\t" + "NA" + "\t" + str(missing_contig_length) + "\n"
		with open(summary_file,"a") as output_to_summary:
			output_to_summary.write(output_for_missing_contigs)




##### Main script #####



### Get assembly file and output prefix from command line arguments ###


parser = argparse.ArgumentParser(description='Runs mash dist on each contig in an assembly and reports best species match for each contig')

parser.add_argument('assembly', metavar='assembly_contigs', type=str,help='assembly file containing contigs')
parser.add_argument('prefix',metavar='prefix',help='prefix for output files')

args = parser.parse_args()

assembly_file = args.assembly
output_prefix = args.prefix




### Define output file and make results folder


output_folder = output_prefix + "_results_folder"

os.makedirs(output_folder)

summary_file = output_folder + "/" + output_prefix + "_summary.txt"



### Loop through assembly and record contig name and size in dictionary


contig_size_dict = {}

contig_sequences = SeqIO.parse(open(assembly_file),'fasta')
for contig in contig_sequences:
	contig_name = contig.id
	contig_seq = str(contig.seq)
	contig_length = len(contig_seq)
	contig_size_dict[contig_name] = contig_length



### Run MASH function and then add results to giant dictionary	
	
	
mash_results_file = run_mash()
		
		
giant_dict = {}

with open(mash_results_file,"rU") as contigs_input_file:
	reader = csv.reader(contigs_input_file,delimiter="\t")
	for row in reader:
		query_contig = row[1]
		matching_hashes = row[4]
		matching_hashes_int = matching_hashes.split("/")[0]
		matching_hashes_int = int(matching_hashes_int)	
		#print matching_hashes_int
		
		if query_contig not in giant_dict:
			giant_dict[query_contig] = [matching_hashes_int]
			
		else:
			dict_value = giant_dict.get(query_contig)
			dict_value.append(matching_hashes_int)
			giant_dict[query_contig] = dict_value

			
print len(giant_dict)


### Make taxa/accession dictionary


taxa_dict = {}

with open("Assembly_accessions_and_organism.csv","rU") as accession_file:
	reader_accession = csv.reader(accession_file,delimiter=",")
	for accession_row in reader_accession:
		accession = accession_row[0]
		taxa = accession_row[1]
		taxa_dict[accession] = taxa
		


### Write details about remaining contigs to summary file
	
best_species_match()



### Write details about missing contigs to summary file


missing_contigs()
