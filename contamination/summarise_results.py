#!/usr/bin/python

### Import Modules ###

import csv
import glob
import operator



### Main script ###



# Write header to output file

with open("Summarised_results.txt","a") as output:
	header = "Sample name\tTotal no. of contigs\tTotal length\tNo. of contigs matching Klebsiella\tTotal length matching Klebsiella\tNo. of contigs not matching Klebsiella\tTotal length not matching Klebsiella\tNo. of contigs with no hits\tTotal length of contigs with not hits\tBest matches ----->\n"
	output.write(header)
	



# Go through summary file and add up the numbers of contigs found (and total length) that match/do not match Klebsiella, and also those with no MASH hits

for file in glob.glob("*_summary.txt"):
	
	sample_name = file.split("_summary.txt")[0]
	print sample_name
	
	no_contigs = 0
	total_length = 0
	klebsiella_count = 0
	klebsiella_length = 0
	non_klebsiella_count = 0
	non_klebsiella_length = 0
	no_hits_count = 0
	no_hits_length = 0
	
	species_contigs_dict = {}
	species_length_dict = {}
	
	with open(file,"rU") as input_file:
		reader = csv.reader(input_file,delimiter="\t")
		for row in reader:
			no_contigs = no_contigs + 1
			contig_name = row[0]
			hit_accession = row[1]
			contig_length = int(row[6])
			total_length = total_length + contig_length
			
			
			if hit_accession != "No hit":
				mash_distance = row[2]
				pvalue = float(row[3])
				matching_hashes = row[4]
				matching_hashes2 = matching_hashes.split("/")[0]
				matching_hashes2 = float(matching_hashes2)
				taxa = row[5]
				genus = taxa.split(" ")[0]
				species = taxa.split(" ")[1]
				overall_taxa = genus + " " + species
				
				if "Klebsiella" in taxa:
					klebsiella_count = klebsiella_count + 1
					klebsiella_length = klebsiella_length + contig_length
					
				else:
					non_klebsiella_count = non_klebsiella_count + 1
					non_klebsiella_length = non_klebsiella_length + contig_length
					
				if overall_taxa not in species_contigs_dict:
					species_contigs_dict[overall_taxa] = 1
				else:
					contigs_value = species_contigs_dict.get(overall_taxa)
					new_contigs_value = contigs_value + 1
					species_contigs_dict[overall_taxa] = new_contigs_value

				if overall_taxa not in species_length_dict:
					species_length_dict[overall_taxa] = contig_length
				else:
					length_value = species_length_dict.get(overall_taxa)
					new_length_value = length_value + contig_length
					species_length_dict[overall_taxa] = new_length_value
				
			else:
				no_hits_count = no_hits_count + 1
				no_hits_length = no_hits_length + contig_length
				
	
	#best_species_key = max(species_length_dict.iteritems(), key=operator.itemgetter(1))[0]
	#best_species_value = species_length_dict.get(best_species_key)
	
	no_contigs = str(no_contigs)
	total_length = str(total_length)
	klebsiella_count = str(klebsiella_count)
	klebsiella_length = str(klebsiella_length)
	non_klebsiella_count = str(non_klebsiella_count)
	non_klebsiella_length = str(non_klebsiella_length)
	no_hits_count = str(no_hits_count)
	no_hits_length = str(no_hits_length)
	
	output_to_file = sample_name + "\t" + no_contigs + "\t" + total_length + "\t" + klebsiella_count + "\t" + klebsiella_length + "\t" + non_klebsiella_count + "\t" + non_klebsiella_length + "\t" + no_hits_count + "\t" + no_hits_length + "\t"
		
	with open("Summarised_results.txt","a") as output:
		output.write(output_to_file)
		
	
	
	# Order the species/taxa in terms of total length found and then write taxa name, contig no. and total length to file to output file
	
	for each_species in sorted(species_length_dict, key=species_length_dict.get, reverse=True):
  		#print each_species, species_length_dict[each_species]
		taxa_result_to_out = each_species
		length_to_out = str(species_length_dict[each_species])
			
		for each_species2, no_contigs in species_contigs_dict.iteritems():
			if each_species2 == each_species:
				contig_no_to_out = str(no_contigs)

					
				with open("Summarised_results.txt","a") as output:
					output.write(taxa_result_to_out)
					output.write("\t")
					output.write(contig_no_to_out)
					output.write("\t")
					output.write(length_to_out)
					output.write("\t")	
	with open("Summarised_results.txt","a") as output:
		output.write("\n")
	
