# GHRU Bioinformatics Workshop 2019 Exercises
This repository contains data and code relating to the workshop

## Dataparsing

 * Example outputs from contamination detection tools and code to parse them:
   * [bbduk](data_parsing/bbduk)
   * [centrifuge](data_parsing/centrifuge)
   * [checkm](data_parsing/checkm)
   * [kraken-uniq](data_parsing/kraken_uniq)
 * General data parsing
   * [parallel processing](data_parsing/general/parallel_code.md)

## Sequence analysis
Two datasets will be used and the Nextflow pipelines can be found [here](https://gitlab.com/cgps/ghru/pipelines).


### Klebsiella pneumoniae
  * [Description](sequence_analysis/Kpn/input_data/K.pneumoniae_sample_set_for_GHRU.xlsx) of input data
  * [Assembly code and outputs](sequence_analysis/Kpn/code_and_outputs/assembly)
  * [Roary code and outputs](sequence_analysis/Kpn/code_and_outputs/roary)
    * Code to link accessions to csv data in [pandas](sequence_analysis/Kpn/code_and_outputs/roary/create_metadata.ipynb)
  * [microreact based on roary](https://microreact.org/project/jtptf1zrh)
  * [Ariba AMR prediction code and outputs](sequence_analysis/Kpn/code_and_outputs/ariba)
  * [SNP phylo code and outputs](sequence_analysis/Kpn/code_and_outputs/snp_phylogeny)
  * Microreacts
    * [No recombination removal](https://microreact.org/project/TsTh13MxG)
    * [With recombination removal](https://microreact.org/project/YeiENXzm9)


### Staphylococcus aureus ST22
  * [Microreact metadata](sequence_analysis/Sau/input_data/microreact-project-EkUvg9uY-data.csv)
  * [Microreact link](https://microreact.org/project/EkUvg9uY?tt=rc)
  * [Accession numbers](sequence_analysis/Sau/input_data/accessions.txt)
  * [Assembly code and outputs](sequence_analysis/Sau/code_and_outputs/assembly)
  * [Roary code and outputs](sequence_analysis/Sau/code_and_outputs/roary)
    * Code to link accessions to microreact data
        * [pandas](sequence_analysis/Sau/code_and_outputs/general/link_ERR_to_microreact_data.ipynb)
        * [R](sequence_analysis/Sau/code_and_outputs/general/link_ERR_to_microreact_data.R)
    * [microreact based on roary](https://microreact.org/project/4iSUXbC4B)
  * [SNP phylo code and outputs](sequence_analysis/Sau/code_and_outputs/snp_phylogeny)
    * [microreact based on SNPs](https://microreact.org/project/rOL5pwv-L)
