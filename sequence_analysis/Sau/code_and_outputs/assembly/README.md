# Assembly of S.aureus ST 22

## assembly
Ran the assembly using [run_assembly.sh](run_assembly.sh)

Reports can be found:
  * [fastqc_multiqc_report.html](fastqc_multiqc_report.html)
  * [qualifyr_report.html](qualifyr_report.html)
  * [quast_multiqc_report.html](quast_multiqc_report.html)

## Assessment

1. Look at reasons for failure using 
    ```
    for f in $(find $PWD -name '*qc*.tsv'); do echo "$(basename $(dirname $f)) $(basename $f)"; cat $f; echo "";done > qualifyr_summary.txt
    ```
    Output is found in [qualifyr_summary.txt](qualifyr_summary.txt)
2. Quast metrics can found in the [output file](combined_transposed_report.tsv). Looking at the [NCBI microbial genome page](https://www.ncbi.nlm.nih.gov/genome/browse#!/prokaryotes/staphylococcus%20aureus)  - 
    Genome sizes range from 2.5 -3.1Mb
3. Based on this all the 'warning' genomes are to be included since N50 scores are acceptable.genomes to exclude are 
  * ERR033620 223 contigs
  * ERR038682 only 1 contig
4.  Copy all fastas into the assemblies/all sub_dir using
  
    `for f in {pass,warning,failure}/*_scaffolds.fasta; do cp $f all/; done`

    Run bactinspector 
    ```
    ~/.local/bin/bactinspector -i all -o bactinspector_output -f '*.fasta' -r ../../../mash/refseq.genomes.k21s1000.msh
    ```

    All look OK except
    ERR038682_scaffolds	Candidatus Tremblaya	20
5. Run roary on all except ERR033620 and ERR038682