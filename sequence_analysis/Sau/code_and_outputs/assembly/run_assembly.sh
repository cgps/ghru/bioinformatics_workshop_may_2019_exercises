nexflow_workflows_dir='/data/nextflow_pipelines/' 
data_dir='/data/ghru_workshop/sau'

nextflow run \
${nexflow_workflows_dir}/assembly/assembly.nf \
--adapter_file ${nexflow_workflows_dir}/assembly/adapters.fas \
--qc_conditions ${nexflow_workflows_dir}/assembly/qc_conditions_nextera.yml \
--input_dir  ${data_dir}/fastqs \
--fastq_pattern '*{R,_}{1,2}.fastq.gz' \
--output_dir ${data_dir}/assembly_output \
--minimum_scaffold_length 1000 \
--depth_cutoff 100 \
-profile standard \
-resume
