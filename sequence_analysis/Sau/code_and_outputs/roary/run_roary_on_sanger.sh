#!/usr/bin/env bash
#BSUB -o /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.e
#BSUB -R "select[mem>1024] rusage[mem=1024]"
#BSUB -M 1024
#BSUB -q basement

/nfs/users/nfs_a/au3/bin/nextflow run /nfs/users/nfs_a/au3/nextflow_workflows/roary/roary.nf \
--input_dir /lustre/scratch118/infgen/team212/au3/saureus_st22/assembly_output/assemblies/qc_passed_assemblies \
--fasta_pattern '*.fasta' \
--output_dir /lustre/scratch118/infgen/team212/au3/saureus_st22/roary_output \
--max_clusters 100000 \
--tree \
-profile sanger \
-resume