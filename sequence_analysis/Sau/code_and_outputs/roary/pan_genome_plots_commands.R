# runs plots for pan genome analysis with roary

core <- read.table(file="number_of_conserved_genes.Rtab", head=FALSE, sep="\t")
summary(core)
col.names <- seq(1,115,1)
names(core) <- col.names
boxplot(core,ylim=c(1000,3000),yaxt="n", ylab="No. of genes", xlab="No. of genomes", main="Core Genes", outline=FALSE)
axis(2, at=seq(1000,3000,500))

pan <- read.table(file="number_of_genes_in_pan_genome.Rtab", head=FALSE, sep="\t")
summary(pan)
names(pan) <- col.names
boxplot(pan,ylim=c(2000,5000),yaxt="n", ylab="No. of genes", xlab="No. of genomes", main="Pan Genome", outline=FALSE)
axis(2, at=seq(2000,5000,500))

unique <- read.table(file="number_of_unique_genes.Rtab", head=FALSE, sep="\t")
summary (unique)
names(unique) <- col.names
boxplot(unique,ylim=c(0,1100),yaxt="n", ylab="No. of unique genes", xlab="No. of genomes", main="Unique Genes", outline=FALSE)
axis(2, at=seq(0,1100,100))

new <- read.table(file="number_of_new_genes.Rtab", head=FALSE, sep="\t")
summary(new)
names(new) <- col.names
boxplot(new,ylim=c(0,3000),yaxt="n", ylab="No. of new genes", xlab="No. of genomes", main="New Genes", outline=FALSE)
axis(2, at=seq(0,3000,500))
