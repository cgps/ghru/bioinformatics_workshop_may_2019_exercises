library(ape)
library(dendextend)

a <- read.tree(file="Knp_roary_tree.nwk")
b <- read.tree(file="Knp_snp_tree.nwk")

//the bipartitions (aka splits) are compared, ff both trees are unrooted and have the same number of nodes.
comparePhylo(a, b, plot = TRUE, force.rooted = TRUE, use.edge.length = F)

library(phytools)
a1<-force.ultrametric(a)
a1<-midpoint.root(a1)
b1<-force.ultrametric(b)
b1<-midpoint.root(b1)
tanglegram(a1,b1, sort=T, highlight_distinct_edges=F, highlight_branches_lwd=F, lab.cex=.7,columns_width= c(7,5,7), dLeaf_left=.05, dLeaf_right=-.05)