#!/usr/bin/env bash
#BSUB -o /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.e
#BSUB -R "select[mem>1024] rusage[mem=1024]"
#BSUB -M 1024
#BSUB -q basement

nexflow_workflows_dir='/nfs/users/nfs_a/au3/nextflow_workflows'
data_dir='/lustre/scratch118/infgen/team212/au3/saureus_st22'

nextflow run ${nexflow_workflows_dir}/snp_phylogeny/snp_phylogeny.nf \
--input_dir ${data_dir}/fastqs \
--fastq_pattern '*{R,_}{1,2}.fastq.gz' \
--reference ${data_dir}/references/GCF_001018835.2_ASM101883v2_genomic.fna \
--output_dir ${data_dir}/snp_phylogeny_output \
--adapter_file ${nexflow_workflows_dir}/snp_phylogeny/adapters.fas \
--depth_cutoff 100 \
--remove_recombination \
--tree \
-resume -profile sanger
