nexflow_workflows_dir='/data/nextflow_pipelines/' 
data_dir='/data/ghru_workshop/sau'


nextflow run ${nexflow_workflows_dir}/snp_phylogeny/snp_phylogeny.nf \
--input_dir ${data_dir}/fastqs \
--fastq_pattern '*{R,_}{1,2}.fastq.gz' \
--reference ${data_dir}/references/GCF_001018835.2_ASM101883v2_genomic.fna \
--output_dir ${data_dir}/snp_phylogeny_output \
--adapter_file ${nexflow_workflows_dir}/snp_phylogeny/adapters.fas \
--depth_cutoff 100 \
--remove_recombination \
--tree \
-resume -profile standard -ansi
