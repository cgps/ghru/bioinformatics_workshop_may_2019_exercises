# Assembly analysis
Got data with 
`pf data --type file --id euscape_kpn_for_ghru_workshop_ids.txt --file-id-type lane --filetype fastq --symlink`

 1. Ran assembly on farm with the [run_assembly.sh](run_assembly.sh) script. Outputs can be found at `/lustre/scratch118/infgen/team212/au3/euscape_kpn_for_ghru_workshop/assembly_output/`
 
    Reports can be found:
    * [fastqc_multiqc_report.html](fastqc_multiqc_report.html)
    * [qualifyr_report.html](qualifyr_report.html)
    * [quast_multiqc_report.html](quast_multiqc_report.html)
 2. Look at reasons for failure using 
    ```
    for f in $(find $PWD -name '*qc*.tsv'); do echo "$(basename $(dirname $f)) $(basename $f)"; cat $f; echo "";done > qualifyr_summary.txt
    ```
    Output is found in [qualifyr_summary.txt](qualifyr_summary.txt)
3. Quast metrics can found in the [output file](combined_transposed_report.tsv). Looking at the [NCBI microbial genome page](https://www.ncbi.nlm.nih.gov/genome/browse#!/prokaryotes/klebsiella%20pneumoniae) the genome size ranges from 5 - 6.4Mb. Therefore suspicious genomes based on size are GR068, BE113, RS097
  * GR068
  * BE113
  * RS097
  Number of contigs for GR022 is also large (193 > 1000bp)
4. Based on this those not to take forward to further analysis are
    * warning
        * None since they are all contig nuber warnings and the worst is 107
    * failure
        * BE079 - contamination
        * GR022 - contamination and possible contamination based on GC
        * GR068 - contamination
        * HU005 - possible contamination based on GC
        * BE113 - contamination
        * RS097 - contamination
        * IE039 - contamination
        * UK079 - contamination
        * RS047 - possible contamination based on GC
5. Copy all fastas into the assemblies/all sub_dir using
  
    `for f in {pass,warning,failure}/*_scaffolds.fasta; do cp $f all/; done`

    Run bactinspector 
    ```
    ~/.local/bin/bactinspector -i all -o bactinspector_output -f '*.fasta' -r ../../../mash/refseq.genomes.k21s1000.msh
    ```

    Output is [species_investigation_2019-05-17.tsv](species_investigation_2019-05-17.tsv)

    Based on this should exclude

    * LT005 (Acinetobacter baumannii)
    * BE113 (Escherichia coli)
    * PL043 (Klebsiella michiganensis)
    * SI005 (Klebsiella variicola)
6. Final list to proceed with is
```
    EuSCAPE_AT004
    EuSCAPE_AT028
    EuSCAPE_BE008
    EuSCAPE_DE032
    EuSCAPE_DE047
    EuSCAPE_ES062
    EuSCAPE_ES065
    EuSCAPE_ES218
    EuSCAPE_ES291
    EuSCAPE_FR078
    EuSCAPE_IL017
    EuSCAPE_IT071
    EuSCAPE_IT118
    EuSCAPE_IT120
    EuSCAPE_IT122
    EuSCAPE_IT124
    EuSCAPE_IT130
    EuSCAPE_IT132
    EuSCAPE_IT133
    EuSCAPE_IT134
    EuSCAPE_IT194
    EuSCAPE_LU002
    EuSCAPE_MT020
    EuSCAPE_PL036
    EuSCAPE_PL047
    EuSCAPE_PT008
    EuSCAPE_PT029
    EuSCAPE_PT062
    EuSCAPE_RO084
    EuSCAPE_RO113
    EuSCAPE_TR013
    EuSCAPE_UK014
    EuSCAPE_UK041
```