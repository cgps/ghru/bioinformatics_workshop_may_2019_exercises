nexflow_workflows_dir='/data/nextflow_pipelines/' 
data_dir='/data/ghru_workshop/kpn'

nextflow run ${nexflow_workflows_dir}/ariba/ariba.nf \
--input_dir ${data_dir}/fastqs/valid_fastqs \
--fastq_pattern '*{R,_}{1,2}.fastq.gz' \
--output_dir ${data_dir}/ariba_output/card_cluster_cols \
--get_database card \
--extra_summary_arguments '--cluster_cols assembled,match,ref_seq,pct_id,ctg_cov,known_var --min_id 90 --col_filter n --row_filter n' \
-resume -profile standard
