nexflow_workflows_dir='/data/nextflow_pipelines/' 
data_dir='/data/ghru_workshop/kpn'

nextflow run ${nexflow_workflows_dir}/roary/roary.nf \
--input_dir ${data_dir}/assembly_output/assemblies/qc_passed_assemblies \
--fasta_pattern '*.fasta' \
--output_dir ${data_dir}/roary_output \
--max_clusters 100000 \
--tree \
-profile standard \
-resume

