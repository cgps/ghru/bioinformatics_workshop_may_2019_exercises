#!/usr/bin/env bash
#BSUB -o /lustre/scratch118/infgen/team212/au3/euscape_kpn_for_ghru_workshop/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/au3/euscape_kpn_for_ghru_workshop/cluster_logs/%J.e
#BSUB -R "select[mem>1024] rusage[mem=1024]"
#BSUB -M 1024
#BSUB -q basement

/nfs/users/nfs_a/au3/bin/nextflow run /nfs/users/nfs_a/au3/nextflow_workflows/roary/roary.nf \
--input_dir /lustre/scratch118/infgen/team212/au3/euscape_kpn_for_ghru_workshop/qc_passed_assemblies \
--fasta_pattern '*.fasta' \
--output_dir /lustre/scratch118/infgen/team212/au3/euscape_kpn_for_ghru_workshop/roary_output \
--max_clusters 100000 \
--tree \
-profile sanger \
-resume