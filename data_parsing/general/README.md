## General coding, UNIX and parsing skills

### Python

 * Lots of good cheat sheest [here][https://ehmatthes.github.io/pcc/cheatsheets/README.html]
 * [Basic cheat sheet](beginners_python_cheat_sheet.pdf)

### Git version control
 * [Simple cheat sheet](https://www.atlassian.com/dam/jcr:8132028b-024f-4b6b-953e-e68fcce0c5fa/atlassian-git-cheatsheet.pdf)
 * [Course in git](https://swcarpentry.github.io/git-novice/)