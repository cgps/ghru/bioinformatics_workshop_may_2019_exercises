## Data Parsing in Unix (bash)

We will learn to use programs that read input, perform a simple transformation and write to the standard output. They are useful to parse and filter our data, and they don't alter the input file in any way.

```
$ program [pattern-action] filename
```

Input -> Transformation -> Output


Commonly used programs:
  * head
  * tail
  * grep - globally search a regular expression and print
  * awk - initials of developers
  * sed - stream editor
  * tr - transpose
  * sort
  * uniq - unique
  * wc - word count
  * comm - common, find lines in common between 2 files
  * join - join two files

You can always find a lot of useful information in the manual pages:

```
$ man program
```

(q to quit)

Open file "data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv"

```
$ cat data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

That is a lot of lines!

#### Count how many lines the file has:

```
$ wc -l data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Look at the first ten lines of file "data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv"

```
$ head data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Look at the last ten lines of file "data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv"

```
$ tail data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```


#### *grep* searches the file and prints each line that contains the pattern.

#### Identify Kraken hits to Klebsiella species

```
$ grep "Klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Now try:

```
$ grep "klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Why no output?

By default, grep is case sensitive!!

But grep has many useful options

-i			Perform case insensitive matching (ignore case)

```
$ grep -i "klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Now try:

```
$ grep "Kleb" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

It worked too!
grep doesn't see "words", just looks for the pattern anywhere on the file

-w			The expression is searched for as a word

```
$ grep -w "Kleb" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

Why no output?

-v			Prints lines not matching the specified pattern

#### Identify Kraken hits to Klebsiella species, excluding the Klebsiella genus line

```
$ grep "Klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | grep -v "genus"
```

-e pattern	Multiple -e options are used to specify multiple patterns

#### Identify Kraken hits to Klebsiella species and Salmonella species

```
$ grep -e "Klebsiella" -e "Salmonella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

-f file (newline separated patterns) Searches all the patterns in a file.  Warning: Empty pattern lines match every input line.

```
$ grep -f target_species.txt data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

*Other useful options*
-m			Stop reading the file after "m" matches  
-H			Print filename headers with output lines  
-A	num		Print num lines of trailing context after each match  
-B	num		Print num lines of leading context before each match  
-C	num		Print num lines of leading and trailing context surrounding each match  

```
$ grep -A 1 "taxReads" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

#### *awk* reads the input one line at the time, each line is compared against the pattern, and for lines that match the pattern an action is performed. 
Either the pattern or the action is optional. If no action is specified awk prints the lines matching the pattern by default.

#### Identify Kraken hits to Klebsiella species (specify pattern, no action)

```
$ awk '/Klebsiella/' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

awk automatically splits the input file into fields. The fields are strings of non-blank characters separated by blanks or tabs. If the separator is specified a tab, then blanks are are not separator characters.

```
$ awk '{print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv 
$ awk '{print $1,$9}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv 
$ awk -F "\t" '{print $1,$9}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv 
```

Above we specified tab as a separator with the option -F. What changed?

#### Identify Kraken hits to Klebsiella species, but print only the % reads (first column)

```
$ awk '/Klebsiella/ {print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv
```

This can also be done combining grep and awk with a pipe (|):

```
$ grep "Klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | awk '{print $1}'
```

#### Identify Kraken hits to Klebsiella species, print only the % reads, and *sort* the values (ascending order by default)

```
$ awk '/Klebsiella/ {print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sort
```

#### Sort them in descending order

```
$ awk '/Klebsiella/ {print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sort -r
```

awk can do calculations for the pattern or for the command.

Let's say I want the absolute number of reads, not the percent of reads. The total number of reads in the fastq file was, for example, 3,500,000.

```
$ grep -w "Klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | awk '{print $1 * 3500000 / 100}' | sort
```
The sort is not by number!

#### Sort numerically and in descending order:
```
$ grep -w "Klebsiella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | awk '{print $1 * 3500000 / 100}' | sort -nr
```

#### Print rows with rank=species and % reads larger than 0.1

```
$ grep -w "species" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | awk '$1 > 0.1 {print }'
Why do we need to pass the option -w to grep?
```

Oops, we don't want species groups, just species.

```
$ grep -w "species" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | grep -v "group" | awk '$1 > 0.1 {print }'
```

#### Print rows with rank=species and % reads larger than 0.1, but only print the % reads and taxName columns

```
$ grep -w "species" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | grep -v "group" |  awk '$1 > 0.1 {print $1,$9,$10}'
```

Why do I need to specify $9 and $10? When you specify fields separated by a comma, the separator becomes a space.

Excellent, we have the information we needed for our summary.

Now for funsies try the same but with "no rank" instead of "species"

```
$ grep -w "no rank" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | awk '{print $1,$9,$10}'
```

We get a different set of columns!

The output of Kraken is messy because it contains tabs and spaces.

awk has a built-in variable called *NF* (number of fields). Because awk reads the input one line at the time, it can report the number of fields on each line.

#### Inspect the kraken output with awk, reporting the number of fields on each line, but looking only at the first 10

```
$ awk '{print NF}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | head
```

You can see that the number of fields per line is quite variable.

#### To get a summary

```
$ awk '{print NF}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sort | uniq
```

It ranges from 0 to 18

*uniq* filters out repeated lines in a file, and only reports a unique set. It also has useful options:
-d      Only output lines that are repeated in the input (a reverse uniq, if you will)
-c      Precede each output line with the count of the number of times the line occurred in the input, followed by a single space
We had to sort between awk and uniq because uniq reads the input file comparing adjacent lines. Repeated lines in the input will not be detected if they are not adjacent.

Now let's use the option -c to get a distribution of the number of fields on each line

```
$ awk '{print NF}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sort | uniq -c 
What's the most frequent number of lines?
```

Ok, so now that we know this output file is a bit messy we can try to tidy it up.
First, let's remove any consecutive number of spaces. We can use sed for that.

*sed* reads lines in the input one at the time, applies commands to each line and writes the edited form to the standard output.

For example:

```
$ sed 's/taxName/Taxon Name/' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | head
```

or 

```
$ grep "Klebsiella pneumoniae" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sed 's/Klebsiella pneumoniae.*/KPN/'
```

Here we used a *regular expression* to specify the pattern "Klebsiella pneumoniae and any other characters after that" by adding ".*"

Now I want to replace "enterica" with "GHRU" (arbitrary, I know) for the Salmonella lines:

```
$ grep "Salmonella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sed 's/enterica/GHRU/'
```

As you can see only the first instance of "enterica" is replaced.

To replace all of the instances of the pattern on each line:

```
$ grep "Salmonella" data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | sed 's/enterica/GHRU/g'
```

So we can use that trick to remove all the consecutive spaces in the file

```
$ sed 's/  //g' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | head -20
```

That removed the consecutive space characters, but we still have a mix of tabs and spaces on some lines. We can use tr to change that.

*tr* copies the standard input to the standard output with substitution or deletion of selected characters. tr needs to take the input from another program via the pipe. It only works for one character.

Remove all the consecutive spaces in the file and then replace single spaces by underscores

```
$ sed 's/  //g' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | tr " " "_"
```

Let's see if this worked by counting the number of fields in the file like before:

```
$ sed 's/  //g' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | tr " " "_" | awk '{print NF}' | sort | uniq -c
```

Whoop! Now we have a tidy file (still with a header) and we can work across different ranks without the issue of the different number of fields.

But I prefer to work with csv files instead of tab files.

Remove all the consecutive spaces in the file, replace single spaces by underscores, and finally replace all tabs by commas:

```
$ sed 's/  //g' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.tsv | tr " " "_" | tr "\t" "," > data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.csv
```

When working with a csv file and awk, you need to specify that the comma is the field separator
Try:

```
$ awk '{print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.csv | head
```

versus

```
$ awk -F, '{print $1}' data_parsing_kraken_uniq_data_10-KPN_S9_mixed_with_1%_1-SAT_S1.krakenuniq.report.csv | head 
```


Congratulations! You now know the basics of how to parse and filter data with bash.


Homework
1. Using file beta-lactamase.fsa and unix commands
    a. Count the number of sequences on the database beta-lactamase.fsa
    b. How many NDM genes are there?
    c. Extract the accession number of CMY-1

2. Using files list_of_lanes.txt and sequence_alignment.fasta and unix commands
    a. Create an alignment containing only the sequences for the lanes in file list_of_lanes.txt (it is a subset of those in sequence_alignment.fasta)


